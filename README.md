# OpenFaaS Static WebApp with Express and Vue.js template

## Status of the template

This template is pre-release and is likely to change

## Trying the template


```
$ faas template pull https://gitlab.com/openfaas-experiments/static-vue-express-template
$ faas new hello-world --lang static-vue-express
```

## Build, push, deploy

```shell
faas-cli build -f ./hello-world.yml
faas-cli push -f ./hello-world.yml
faas-cli deploy -f ./hello-world.yml
```
Or:
```shell
faas-cli up -f ./hello-world.yml
```



